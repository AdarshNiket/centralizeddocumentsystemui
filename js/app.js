var app = angular.module('docManager',['ngRoute']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/',{
            templateUrl: './js/login/partial/login.html',
            controller: 'LoginController'
        })
        .when('/register', {
            templateUrl: './js/register/partial/registration.html',
            controller: 'RegisterController'
        })
        .when('/admin',{
            templateUrl: './js/admin/partial/admin.html',
            controller: 'AdminController'
        })
        .when('/home', {
            templateUrl: './js/user/partial/landing.html',
            controller: 'UserController'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);